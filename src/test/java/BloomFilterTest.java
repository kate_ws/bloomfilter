import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BloomFilterTest {

    /**
     * Actually shows, that on 100000000 instances there is only 2204 errors
     */
    @Test
    void bfTest() {
        BloomFilter blFl = new BloomFilter();
        IntStream.range(0, 100000000).mapToObj(i -> "test" + i).forEachOrdered(blFl::put);

        var count = (int) IntStream.range(0, 100000000)
                .mapToObj(i -> "test" + i)
                .filter(blFl::contains)
                .count();
        assertEquals(count , 100000000);

        count = (int) IntStream.range(0, 100000000)
                .mapToObj(i -> "test" + i)
                .filter(blFl::contains)
                .count();
        assertEquals(count , 100002204);
    }
}