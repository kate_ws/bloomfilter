import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.IntStream;

public class BloomFilter {

    private static final int[] salts = {3, 5, 7, 11, 13, 17, 19, 23, 29};
    private final boolean[] bitMap = new boolean[Integer.MAX_VALUE - 2];


    public BloomFilter() {
        IntStream.range(0, Integer.MAX_VALUE - 2)
                .forEachOrdered(integer -> bitMap[integer] = false);
    }


    public void put(String target) {
        if (StringUtils.isEmpty(target)) {
            throw new RuntimeException("empty string");

        } else {
            Arrays.stream(salts)
                    .forEach(salt -> bitMap[customHash(target, salt)] = true);
        }

    }

    public boolean contains(String target) {
        if (StringUtils.isEmpty(target)) {
            return false;

        } else {
            return Arrays.stream(salts)
                    .allMatch(salt -> bitMap[customHash(target, salt)]);
        }
    }

    public static int customHash(String stringTarget, int salt) {
        if (StringUtils.isEmpty(stringTarget)) {
            throw new RuntimeException("empty string");

        } else {
            var hash = 0;
            for (var position = 0; position < stringTarget.length(); position++) {
                hash = salt * hash + stringTarget.charAt(position);
            }
            return Integer.MAX_VALUE & hash;
        }
    }

    /**
     * Actually shows, that on 100000000 instances there is only 2204 errors
     *
     * main method could be used, when test runs out of memory, actually there is no need in it
     * actually it is ready to use as an external library
     */
    public static void main(String[] args) {
        BloomFilter blFl = new BloomFilter();
        IntStream.range(0, 100000000).mapToObj(integer -> "test" + integer).forEachOrdered(blFl::put);

        var count = (int) IntStream.range(0, 100000000)
                .mapToObj(integer -> "test" + integer)
                .filter(blFl::contains)
                .count();
        System.out.println(count);

        count = (int) IntStream.range(0, 1000000000)
                .mapToObj(integer -> "test" + integer)
                .filter(blFl::contains)
                .count();
        System.out.println(count);
    }



}
